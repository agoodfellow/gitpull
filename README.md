## GitPull

A simple bash script for pulling multiple git repositories in a folder at once. For Linux only.

## How to use:

Just put the gitpull file inside of the folder that you want to update the git repositories and run it.

If you want to ignore some repositories add them to the ignored list inside of double quotes with single spaces between them in the gitpull file.

## Special thanks to:

[Aaron Okano](https://stackoverflow.com/users/2757435/aaron-okano) and [Westy92](https://stackoverflow.com/users/453314/westy92) from stackoverflow for the folders line.

[user708516](https://stackoverflow.com/users/708516/user708516) and [Community(bot)](https://stackoverflow.com/users/-1/community) from stackoverflow for the for loop part.

[0x00](https://stackoverflow.com/users/6949968/0x00) and [Steven Shaw](https://stackoverflow.com/users/482382/steven-shaw) from stackoverflow for the first two parts of the echo line.

[Dmitry Mitskevich](https://stackoverflow.com/users/1087874/dmitry-mitskevich) from stackoverflow for the last part of the echo line.

I don't know much bash and without these people all i had was an concept of what i want to do.

## License:

This program is licensed under [3-Clause BSD License](https://opensource.org/licenses/BSD-3-Clause). You are free to do whatever you want to do with it.